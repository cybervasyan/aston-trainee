function concatStrings(string, separator) {
    return (secondString) => {
        if (typeof secondString !== 'string') {

            if (separator != undefined){
                return string + separator;
            }else{
                return string;
            }
        }

        if (separator != undefined){
            return concatStrings(string + separator + secondString);
        }else{
            return concatStrings(string + secondString);
        }

    }
}