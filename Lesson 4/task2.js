class Calculator {
    constructor(numberOne, numberTwo) {
        if (arguments.length < 2 || isNaN(numberOne) || isNaN(numberTwo)) {
            throw new Error("Both of numbers have to be valid");
        }
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
    }

    setX(number) {
        this.numberOne = number;
    }

    setY(number) {
        this.numberTwo = number;
    }

    get logSum() {
        const t = this;
        return () => {
            console.log(t.numberOne + t.numberTwo);
        }
    }

    get logMul() {
        const t = this;
        return () => {
            console.log(t.numberOne * t.numberTwo);
        }
    }

    get logSub() {
        const t = this;
        return () => {
            console.log(t.numberOne - t.numberTwo);
        }
    }

    get logDiv() {
        const t = this;
        return () => {
            if (this.numberTwo === 0) {
                throw new Error("You can't divide by zero");
            }
            console.log(t.numberOne / t.numberTwo);
        }
    }

}