function makeObjectDeepCopy(objectToCopy){
    const createdObject = {};
    for (let key in objectToCopy) {
        if (typeof objectToCopy[key] === 'object'){
            createdObject[key] = makeObjectDeepCopy(objectToCopy[key]);
        }else{
            createdObject[key] = objectToCopy[key];
        }
    }

    return createdObject;
}