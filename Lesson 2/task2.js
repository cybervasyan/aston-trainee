function selectFromInterval(array, start, end){
    let min;
    let max;

    const isValidArray =
        Array.isArray(array)
        && array.length > 0
        && array.every(
            (element) => typeof element === 'number' && !isNaN(element)
        );

    const isValidStartAndEnd = Number.isInteger(start) && Number.isInteger(end);

    if (!isValidStartAndEnd){
        throw new Error("Start and end have to be valid integers")
    } else {
        min = Math.min(start, end);
        max = Math.max(start, end);
    }

    if (!isValidArray){
        throw new Error("Invalid array")
    } else {
        return array.filter((element) => element >= min && element <= max);
    }
}