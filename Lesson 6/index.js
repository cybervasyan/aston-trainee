const textIcons = document.querySelectorAll('.text-icon');

textIcons.forEach(icon => {
    const textEl = icon.previousElementSibling;
    const parentEl = icon.parentElement;

    resize(icon, textEl);
    const observer = new ResizeObserver(() => {
        resize(icon, textEl);
    });
    observer.observe(parentEl);
});

function resize(icon, text) {
    icon.style.left = text.offsetWidth + 16 + 'px';
}

