function createDebounceFunction(callback, ms){
    let isAvailable = true;
    let timeout;

    return function () {
        if (isAvailable) {
            timeout = setTimeout(() => {
                callback.apply(this, arguments);
                isAvailable = true;
            }, ms);

            isAvailable = false;
        }else {
            clearTimeout(timeout);
            timeout = setTimeout(callback, ms);
        }
    };

}