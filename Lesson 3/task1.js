Array.prototype.myFilter = function (callback, context) {

    const result = [];
    let myFuncContext = this;

    if (arguments.length > 1) {
        myFuncContext = context;
    }

    for (let i = 0; i < this.length; i++) {
        if (i in myFuncContext) {
            let current = myFuncContext[i];
            if (callback(current, i, myFuncContext)) {
                result.push(current);
            }
        }
    }

    return result;
}