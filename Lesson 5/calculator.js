export default class Calculator {
    constructor(previousOperandTextElement, currentOperandTextElement) {
        this.previousOperandTextElement = previousOperandTextElement;
        this.currentOperandTextElement = currentOperandTextElement;
        this.clear();
    }

    clear() {
        this.currentOperand = '';
        this.previousOperand = '';
        this.operation = undefined;
    }

    delete() {
        this.currentOperand = this.currentOperand.slice(0, -1);
    }

    appendNumber(number) {
        if (number === '.' && this.currentOperand.includes('.')) return;
        if (number === '.') this.currentOperand = 0;
        this.currentOperand = this.currentOperand.toString() + number.toString();
    }

    chooseOperation(operation) {
        if (this.currentOperand === '') return;
        if (this.previousOperand !== '') {
            this.compute();
        }
        this.operation = operation;
        this.previousOperand = this.currentOperand;
        this.currentOperand = '';
    }

    compute() {
        let computation;
        let prev = parseFloat(this.previousOperand);
        let current = parseFloat(this.currentOperand);
        if (isNaN(current)) current = parseFloat(this.previousOperand);
        if (isNaN(prev) || isNaN(current)) return;
        switch (this.operation) {
            case '+':
                computation = prev + current;
                break;
            case '-':
                computation = prev - current;
                break;
            case '*':
                computation = prev * current;
                break;
            case '÷':
                computation = prev / current;
                break;
            case 'pow':
                computation = Math.pow(prev, current);
                break;
            default:
                return;
        }

        this.currentOperand = this.round(computation);
        this.operation = undefined;
        this.previousOperand = '';

    }

    getDisplayNumber(number) {
        if (isNaN(number) || !isFinite(number)) {
            return 'Err';
        }
        const stringNumber = number.toString();
        const integerDigits = parseFloat(stringNumber.split('.')[0]);
        const decimalDigits = stringNumber.split('.')[1];
        let integerDisplay;
        if (isNaN(integerDigits)) {
            integerDisplay = '';
        } else {
            integerDisplay = integerDigits.toLocaleString('ru');
        }
        if (decimalDigits != null) {
            return `${integerDisplay}.${decimalDigits}`;
        } else {
            return integerDisplay;
        }
    }

    updateDisplay() {
        this.currentOperandTextElement.innerText = this.getDisplayNumber(this.currentOperand);
        if (this.operation != null) {
            this.previousOperandTextElement.innerText =
                `${this.getDisplayNumber(this.previousOperand)} ${this.operation}`;
        } else {
            this.previousOperandTextElement.innerText = '';
        }
    }

    sidePanelCompute(operation){
        switch(operation){
            case 'sin':
                return this.round(Math.sin(this.currentOperand));
            case 'cos':
                return this.round(Math.cos(this.currentOperand));
            case 'tan':
                return this.round(Math.tan(this.currentOperand));
            case 'sqrt':
                return this.round(Math.sqrt(this.currentOperand));
            case 'x²':
                return this.round(Math.pow(this.currentOperand, 2));
            case 'x!':
                return this.factorial(this.currentOperand);
            case 'cot':
                return this.round(1 / Math.tan(this.currentOperand));
            case '1/x':
                return this.round(1 / this.currentOperand);
            case 'e':
                return this.round(Math.exp(this.currentOperand));
            default:
                return;
        }
    }

    round(number) {
        return Math.ceil(number * 100000000) / 100000000;
    }

    factorial(n) {
        return (n !== 1) ? n * this.factorial(n - 1) : 1;
    }
}
