import Calculator from "./calculator.js";

const numberButtons = document.querySelectorAll('[data-number]');
const operationButtons = document.querySelectorAll('[data-operation]');
const equalsButton = document.querySelector('[data-equals]');
const deleteButton = document.querySelector('[data-delete]');
const allClearButton = document.querySelector('[data-all-clear]');
const percentButton = document.querySelector('[data-percent]');
const plusMinusButton = document.querySelector('[data-plus-minus]');
const sidePanelButtons = document.querySelectorAll('[data-side-panel]');
const previousOperandTextElement = document.querySelector('[data-previous-operand]');
const currentOperandTextElement = document.querySelector('[data-current-operand]');


const calculator = new Calculator(previousOperandTextElement, currentOperandTextElement);

numberButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.appendNumber(button.innerText);
        calculator.updateDisplay();
    })
})

operationButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.chooseOperation(button.innerText);
        calculator.updateDisplay();
    })
})

equalsButton.addEventListener('click', () => {
    calculator.compute();
    calculator.updateDisplay();
})

allClearButton.addEventListener('click', () => {
    calculator.clear();
    calculator.updateDisplay();
})

deleteButton.addEventListener('click', () => {
    calculator.delete();
    calculator.updateDisplay();
})

percentButton.addEventListener('click', () => {
    calculator.currentOperand = calculator.currentOperand / 100;
    calculator.updateDisplay();
})

plusMinusButton.addEventListener('click', () => {
    calculator.currentOperand = -calculator.currentOperand;
    calculator.updateDisplay();
})

sidePanelButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.currentOperand = calculator.sidePanelCompute(button.innerText);
        calculator.updateDisplay();
    })
})