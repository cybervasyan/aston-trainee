function getSumAndQuotient(){
    const firstNumber = parseInt(prompt("Введите первое число"));

    if (isNaN(firstNumber)){
        throw new Error("Некорректный ввод!");
    }

    const secondNumber = parseInt(prompt("Введите второе число"));

    if (isNaN(secondNumber)){
        throw new Error("Некорректный ввод!");
    }

    console.log(`Ответ: ${firstNumber + secondNumber}, ${firstNumber / secondNumber}`)
}